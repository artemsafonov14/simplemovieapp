package com.example.mobilka6

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mobilka6.adapters.ContentAdapter
import com.example.mobilka6.models.Content
import com.example.mobilka6.presenter.ResponsePresenter
import com.example.mobilka6.view.ResponseView
import kotlinx.android.synthetic.main.activity_main.*
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class MainActivity : MvpAppCompatActivity(), ResponseView {
    @InjectPresenter
    lateinit var responsePresenter: ResponsePresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rv_vertical.layoutManager = LinearLayoutManager(this)
        rv_vertical.setHasFixedSize(true)
    }
    override fun showContentData(items: List<Content>) {
        rv_vertical.adapter = ContentAdapter(items,this)
    }
    override fun showLoading(isLoad: Boolean) {
        if (isLoad) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE
    }
    override fun showError(message: String) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }
}