package com.example.mobilka6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_movie.*

class MovieActivity : AppCompatActivity() {
    private val IMAGE_BASE = "https://www.signalmediacorp.com//b/c/"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)
        val title = intent.getStringExtra("title")
        movie_title.text = title
        val genre = intent.getStringExtra("genre")
        genre_title.text = genre
        val created = intent.getStringExtra("created")
        movie_created.text = created
        val language = intent.getStringExtra("language")
        language_movie.text = language
        val image = intent.getStringExtra("image")
        Glide.with(this).load("$IMAGE_BASE$image.jpg").into(this.single_movie_poster)
    }
}