package com.example.mobilka6.adapters

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilka6.MovieActivity
import com.example.mobilka6.R
import com.example.mobilka6.models.Content
import com.example.mobilka6.models.Movie
import com.example.mobilka6.services.MovieItemClickListener
import kotlinx.android.synthetic.main.horizontal_rv.view.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class ContentAdapter(private val contents: List<Content>, private val mContext: Context): RecyclerView.Adapter<ContentAdapter.ContentViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentViewHolder {
        return ContentViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.horizontal_rv, parent, false)
        )
    }
    override fun onBindViewHolder(holder: ContentViewHolder, position: Int){
        holder.bindContent(contents[position])
    }
    override fun getItemCount() = contents.size
    inner class ContentViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bindContent(content: Content) {
            itemView.genre_title.text = content.title
            val movieAdapter = MovieAdapter(content.movies, object : MovieItemClickListener{
                @RequiresApi(Build.VERSION_CODES.O)
                override fun onItemClick(item: Movie, position: Int) {
                    val stringFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
                    val dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")
                    val date = LocalDate.parse(item.createdAt, stringFormatter)
                    val dateString = date.format(dateFormatter)
                    val intent = Intent(mContext, MovieActivity::class.java)
                    intent.putExtra("title", item.title_movie)
                    intent.putExtra("genre", item.genre?.title)
                    intent.putExtra("created", dateString )
                    intent.putExtra("image", item.image.idImage)
                    intent.putExtra("language", item.languages?.joinToString(", ") { language -> language.titleLanguage })
                    mContext.startActivity(intent)
                }
            })
            itemView.rv_movies_list.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
            itemView.rv_movies_list.adapter = movieAdapter
        }
    }
}