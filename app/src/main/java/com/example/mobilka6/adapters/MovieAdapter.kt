package com.example.mobilka6.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mobilka6.R
import com.example.mobilka6.models.Movie
import com.example.mobilka6.services.MovieItemClickListener
import kotlinx.android.synthetic.main.movie_item.view.*
import kotlinx.android.synthetic.main.movie_item.view.movie_title

class MovieAdapter(private val movies: ArrayList<Movie>, private val clickListener: MovieItemClickListener): RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
        )
    }
    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bindMovie(movies[position], clickListener)
    }
    override fun getItemCount() = movies.size
    inner class MovieViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        private val IMAGE_BASE = "https://www.signalmediacorp.com//b/c/"
        fun bindMovie(movie: Movie, action: MovieItemClickListener) {
            itemView.movie_title.text = movie.title_movie
            Glide.with(itemView).load(IMAGE_BASE + movie.image.idImage + ".jpg").into(itemView.movie_poster)
            itemView.setOnClickListener {
                action.onItemClick(movie, adapterPosition)
            }
        }
    }
}