package com.example.mobilka6.models

import com.google.gson.annotations.SerializedName


data class Content(
    val id: String?,
    val title: String?,
    @SerializedName("content")
    val movies: ArrayList<Movie>
)