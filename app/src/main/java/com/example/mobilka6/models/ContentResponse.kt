package com.example.mobilka6.models

import com.google.gson.annotations.SerializedName


data class ContentResponse(
    @SerializedName("content")
    val content : List<Content>
)