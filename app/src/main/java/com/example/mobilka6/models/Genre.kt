package com.example.mobilka6.models

import com.google.gson.annotations.SerializedName

data class Genre(
    @SerializedName("title")
    val title: String?
)