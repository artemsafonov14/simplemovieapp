package com.example.mobilka6.models


import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("id")
    val idImage: String?
)