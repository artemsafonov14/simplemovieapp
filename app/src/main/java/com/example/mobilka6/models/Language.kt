package com.example.mobilka6.models

import com.google.gson.annotations.SerializedName

data class Language(
    @SerializedName("title")
    val titleLanguage: String
)