package com.example.mobilka6.models

import com.google.gson.annotations.SerializedName

data class Movie(
    @SerializedName("id")
    val id_movie: String,
    @SerializedName("title")
    val title_movie: String,
    @SerializedName("cover")
    val image: Image,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("genre")
    val genre: Genre?,
    @SerializedName("languages")
    val languages: List<Language>?
)