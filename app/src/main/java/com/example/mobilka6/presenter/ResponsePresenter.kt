package com.example.mobilka6.presenter

import com.example.mobilka6.models.ContentResponse
import com.example.mobilka6.services.ContentInterface
import com.example.mobilka6.view.ResponseView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import moxy.MvpPresenter
import retrofit2.*
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@InjectViewState
class ResponsePresenter : MvpPresenter<ResponseView>() {
    private val BASE_URL = "https://www.signalmediacorp.com"
    private var retrofit : Retrofit? = null
    private val service = getInstance().create(ContentInterface::class.java)
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showLoading(true)
        val compositeDisposable = CompositeDisposable()
        compositeDisposable.add(
        service.getContentList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response -> onResponse(response) }, { t -> onFailure(t) }))
    }
    private fun getInstance() : Retrofit{
        if (retrofit == null){
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        }
        return retrofit!!
    }
    private fun onResponse(response: ContentResponse) {
        viewState.showLoading(false)
        viewState.showContentData(response.content)
    }
    private fun onFailure(t: Throwable) {
        viewState.showLoading(false)
        viewState.showError(t.toString())
    }
}