package com.example.mobilka6.services

import com.example.mobilka6.models.ContentResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface ContentInterface {
    @GET("/api/main_page")
    fun getContentList(): Observable<ContentResponse>
}