package com.example.mobilka6.services

import com.example.mobilka6.models.Movie

interface MovieItemClickListener {
    fun onItemClick(item: Movie, position: Int)
}