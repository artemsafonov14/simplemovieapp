package com.example.mobilka6.view

import com.example.mobilka6.models.Content
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface ResponseView : MvpView {
    fun showContentData(items: List<Content>)
    fun showLoading(isLoad: Boolean)
    fun showError(message: String)
}